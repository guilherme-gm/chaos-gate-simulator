import { Key } from "./Key.js";
import { GameState, MaxWords } from "./GameState.js";
import { InputManager } from "./InputManager.js";
import { SoundPlayer, Sounds } from "./SoundPlayer.js";

export class WordController {
	constructor() {
		this.closed = false; //<- só pra simular um destroy

		this.isConfirm = false;
		this.cursor = { col: 1, row: 0 };
		this.yesNoRow = 0;

		// Define os boundaries da seleção
		this.minColumn = 1;
		this.maxColumn = MaxWords + 1;

		// Estado interno da janela (não afeta o global até o submit final)
		this.selection = [];
		for (let i = 0; i < this.maxColumn + 1; i++) {
			this.selection.push(0);
		}

		// Aplica ajustes se for um mapa que permite troca de server
		if (GameState.canSelectServer) {
			this.cursor.col = 0;
			this.minColumn = 0;
		}
	}

	/* private */ inRange(val, min, max) {
		return val >= min && val < max;
	}

	/* private */ getMaxColumnRows(col) {
		if (col === 0) {
			if (GameState.canSelectServer) {
				return GameState.serverList.length;
			}

			return 1;
		}

		return GameState.wordList[col - 1].length;
	}

	/**
	 * Encerra a tela, aqui iria o código pra destruir os objetos
	 */
	/* private */ destroy(success) {
		alert(`O menu foi fechado\nNovas words? ${success}\nWords do GlobalState: ${GameState.selectedServer}, ${GameState.selectedWords[0]?.label}, ${GameState.selectedWords[1]?.label}, ${GameState.selectedWords[2]?.label}`);
		this.closed = true;
	}

	/**
	 * Processa a decisão de Yes/No para o menu de confirm.
	 * Aqui é onde o estado interno é persistido e então a janela destruída
	 */
	/* private */ onYesNoResponse() {
		if (this.yesNoRow == 1) {
			this.isConfirm = false;
			return;
		}

		if (GameState.canSelectServer) {
			GameState.selectedServer = GameState.serverList[this.selection[0]];
		}

		for (let i = 0; i < MaxWords; i++) {
			GameState.selectedWords[i] = GameState.wordList[i][this.selection[i + 1]];
		}

		this.destroy(true);
	}

	/**
	 * Atualiza o estado interno da janela de confirmação.
	 * Recebe o conjunto de mudanças de cursor a ser aplicado
	 */
	/* private */ updateConfirmWindow(change) {
		if (change.colChange != 0) {
			return false;
		}

		if (change.rowChange != 0 && this.inRange(this.yesNoRow + change.rowChange, 0, 2)) {
			this.yesNoRow += change.rowChange;
			return true;
		}

		return false;
	}

	/**
	 * Atualiza o estado interno do seletor de palavras.
	 * Recebe o conjunto de mudanças a ser aplicada
	 */
	/* private */ updateWordSelector(change) {
		if (change.colChange != 0 && this.inRange(this.cursor.col + change.colChange, this.minColumn, this.maxColumn)) {
			this.cursor.col += change.colChange;
			this.cursor.row = this.selection[this.cursor.col];
			return true;
		}

		if (change.rowChange != 0) {
			const lastRow = this.getMaxColumnRows(this.cursor.col) - 1;
			this.cursor.row += change.rowChange;

			if (this.cursor.row > lastRow) {
				this.cursor.row = 0;
			} else if (this.cursor.row < 0) {
				this.cursor.row = lastRow;
			}

			this.selection[this.cursor.col] = this.cursor.row;
			return true;
		}

		return false;
	}

	/**
	 * Processa inputs em geral.
	 * Para inputs que levam a movimentaçao do cursor, retorna esse movimento, caso contrário, null.
	 */
	/* private */ getInput() {
		if (InputManager.isPressing(Key.ARROW_UP)) {
			SoundPlayer.play(Sounds.Menu_Move);
			return { rowChange: -1, colChange: 0 };
		}

		if (InputManager.isPressing(Key.ARROW_DOWN)) {
			SoundPlayer.play(Sounds.Menu_Move);
			return { rowChange: 1, colChange: 0 };
		}

		if (InputManager.isPressing(Key.ARROW_LEFT)) {
			SoundPlayer.play(Sounds.Menu_Move);
			return { rowChange: 0, colChange: -1 };
		}

		if (InputManager.isPressing(Key.ARROW_RIGHT)) {
			SoundPlayer.play(Sounds.Menu_Move);
			return { rowChange: 0, colChange: 1 };
		}

		if (InputManager.isPressing(Key.Z)) {
			SoundPlayer.play(Sounds.Menu_Confirm);
			if (this.isConfirm) {
				this.onYesNoResponse();
				return null;
			}

			if (this.cursor.col === this.maxColumn - 1) {
				this.isConfirm = true;
				return null;
			}

			return { rowChange: 0, colChange: 1 };
		}

		if (InputManager.isPressing(Key.X)) {
			SoundPlayer.play(Sounds.Menu_Cancel);
			if (this.isConfirm) {
				this.isConfirm = false;
				this.yesNoRow = 0;
				return null;
			}

			if (this.cursor.col === this.minColumn) {
				this.destroy(false);
				return null;
			}

			return { rowChange: 0, colChange: -1 };
		}

		return null;
	}

	/**
	 * Função auxiliar para checar o estado interno.
	 * Retorna se uma linha/coluna está selecionada
	 */
	/* public */ isSelected(col, row) {
		return this.selection[col] === row;
	}

	/**
	 * Equivalente ao render, é parte do loop de renderização. Vai ser executado a cada loop.
	 */
	/* public */ step() {
		const change = this.getInput();
		if (change == null) {
			return;
		}

		if (!this.isConfirm) {
			this.updateWordSelector(change);
		} else {
			this.updateConfirmWindow(change);
		}
	}
}
