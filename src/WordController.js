import { Key } from "./Key.js";
import { GameState, MaxWords } from "./GameState.js";
import { InputManager } from "./InputManager.js";
import { SoundPlayer, Sounds } from "./SoundPlayer.js";

/**
 * Lista de acoes possíveis
 */
const Actions = {
	None: 0,
	MoveUp: 1,
	MoveDown: 2,
	MoveLeft: 3,
	MoveRight: 4,
	Ok: 5,
	Cancel: 6,
};

export class WordController {
	constructor() {
		this.closed = false; //<- só pra simular um destroy

		this.isConfirm = false;
		this.cursor = { col: 1, row: 0 };
		this.yesNoRow = 0;

		// Define os boundaries da seleção
		this.minColumn = 1;
		this.maxColumn = MaxWords + 1;

		// Estado interno da janela (não afeta o global até o submit final)
		this.selection = [];
		for (let i = 0; i < this.maxColumn + 1; i++) {
			this.selection.push(0);
		}

		// Aplica ajustes se for um mapa que permite troca de server
		if (GameState.canSelectServer) {
			this.cursor.col = 0;
			this.minColumn = 0;
		}
	}

	/* private */ inRange(val, min, max) {
		return val >= min && val < max;
	}

	/* private */ getMaxColumnRows(col) {
		if (col === 0) {
			if (GameState.canSelectServer) {
				return GameState.serverList.length;
			}

			return 1;
		}

		return GameState.wordList[col - 1].length;
	}

	/**
	 * Converte ações em movimentos do cursor.
	 */
	/* private */ actionToMovement(action) {
		switch (action) {
		case Actions.MoveUp:
			return { colChange: 0, rowChange: -1 };
		
		case Actions.MoveDown:
			return { colChange: 0, rowChange: 1 };
		
		case Actions.MoveLeft:
		case Actions.Cancel:
			return { colChange: -1, rowChange: 0 };
		
		case Actions.MoveRight:
		case Actions.Ok:
			return { colChange: 1, rowChange: 0 };
		}

		return { colChange: 0, rowChange: 0 };
	}

	/**
	 * Encerra a tela, aqui iria o código pra destruir os objetos
	 */
	/* private */ destroy(success) {
		alert(`O menu foi fechado\nNovas words? ${success}\nWords do GlobalState: ${GameState.selectedServer}, ${GameState.selectedWords[0]?.label}, ${GameState.selectedWords[1]?.label}, ${GameState.selectedWords[2]?.label}`);
		this.closed = true;
	}

	/**
	 * Processa a decisão de Yes/No para o menu de confirm.
	 * Aqui é onde o estado interno é persistido e então a janela destruída
	 */
	/* private */ onYesNoResponse() {
		if (this.yesNoRow == 1) {
			this.isConfirm = false;
			return;
		}

		if (GameState.canSelectServer) {
			GameState.selectedServer = GameState.serverList[this.selection[0]];
		}

		for (let i = 0; i < MaxWords; i++) {
			GameState.selectedWords[i] = GameState.wordList[i][this.selection[i + 1]];
		}

		this.destroy(true);
	}

	/**
	 * Aplica a ação sobre a janela de confirmação.
	 * Atualizando o estado interno.
	 */
	/* private */ actOnConfirmWindow(action) {
		if (action === Actions.Ok) {
			this.onYesNoResponse();
			return;
		}

		if (action === Actions.Cancel) {
			this.yesNoRow = 0;
			this.isConfirm = false;
			return;
		}

		const change = this.actionToMovement(action);
		if (change.colChange != 0) {
			return;
		}

		if (change.rowChange != 0 && this.inRange(this.yesNoRow + change.rowChange, 0, 2)) {
			this.yesNoRow += change.rowChange;
			return;
		}
	}

	/**
	 * Aplica a ação sobre a janela de seleção de palavras.
	 * Atualizando o estado interno.
	 */
	/* private */ actOnWordSelector(action) {
		if (action === Actions.Cancel && this.cursor.col === this.minColumn)  {
			this.destroy(false);
			return;
		}

		if (action === Actions.Ok && this.cursor.col === this.maxColumn - 1) {
			this.isConfirm = true;
			return;
		}

		const change = this.actionToMovement(action);
		if (change.colChange != 0 && this.inRange(this.cursor.col + change.colChange, this.minColumn, this.maxColumn)) {
			this.cursor.col += change.colChange;
			this.cursor.row = this.selection[this.cursor.col];
			return true;
		}

		if (change.rowChange != 0) {
			const lastRow = this.getMaxColumnRows(this.cursor.col) - 1;
			this.cursor.row += change.rowChange;

			if (this.cursor.row > lastRow) {
				this.cursor.row = 0;
			} else if (this.cursor.row < 0) {
				this.cursor.row = lastRow;
			}

			this.selection[this.cursor.col] = this.cursor.row;
			return true;
		}

		return false;
	}

	/**
	 * Processa inputs em geral e retorna a ação tomada
	 */
	/* private */ getAction() {
		if (InputManager.isPressing(Key.ARROW_UP)) {
			SoundPlayer.play(Sounds.Menu_Move);
			return Actions.MoveUp;
		}

		if (InputManager.isPressing(Key.ARROW_DOWN)) {
			SoundPlayer.play(Sounds.Menu_Move);
			return Actions.MoveDown;
		}

		if (InputManager.isPressing(Key.ARROW_LEFT)) {
			SoundPlayer.play(Sounds.Menu_Move);
			return Actions.MoveLeft;
		}

		if (InputManager.isPressing(Key.ARROW_RIGHT)) {
			SoundPlayer.play(Sounds.Menu_Move);
			return Actions.MoveRight;
		}

		if (InputManager.isPressing(Key.Z)) {
			SoundPlayer.play(Sounds.Menu_Confirm);
			return Actions.Ok;
		}

		if (InputManager.isPressing(Key.X)) {
			SoundPlayer.play(Sounds.Menu_Cancel);
			return Actions.Cancel;
		}

		return Actions.None;
	}

	/**
	 * Função auxiliar para checar o estado interno.
	 * Retorna se uma linha/coluna está selecionada
	 */
	/* public */ isSelected(col, row) {
		return this.selection[col] === row;
	}

	/**
	 * Equivalente ao render, é parte do loop de renderização. Vai ser executado a cada loop.
	 */
	/* public */ step() {
		const action = this.getAction();
		if (action === Actions.None) {
			return;
		}

		if (!this.isConfirm) {
			this.actOnWordSelector(action);
		} else {
			this.actOnConfirmWindow(action);
		}
	}
}
