// Isso seria um json com as palavras, o efeito delas poderia ser definido aqui tbm.
export default {
    /* Word 0 */ "0": [
        { "label": "Broken" },
        { "label": "Cold" },
        { "label": "Cursed" },
        { "label": "Dangerous" },
        { "label": "Dead" },
        { "label": "Desolate" },
        { "label": "Destroyed" }
    ],
    /* Word 1 */ "1": [
        { "label": "Chained" },
        { "label": "Collective" },
        { "label": "Common" },
        { "label": "Contaminated" },
        { "label": "Crimson" },
        { "label": "Dark" },
        { "label": "Exploding" }
    ],
    /* Word 2 */ "2": [
        { "label": "Conscient" },
        { "label": "Corruption" },
        { "label": "Destiny" },
        { "label": "Determination" },
        { "label": "Eclipse" },
        { "label": "Fantasy" },
        { "label": "Farm" }
    ]
}