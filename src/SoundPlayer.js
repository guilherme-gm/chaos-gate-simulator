export const Sounds = {
	Menu_Move: 'menuMove.mp3',
	Menu_Confirm: 'confirm.mp3',
	Menu_Cancel: 'cancel.mp3',
};

export class SoundPlayer {
	static init() {
		SoundPlayer.SoundLib = {};

		const body = document.getElementsByTagName('body')[0];
		Object.values(Sounds).forEach((file) => {
			const ele = document.createElement('audio');
			ele.src = `assets/${file}`;
			body.appendChild(ele);

			SoundPlayer.SoundLib[file] = ele;
		});
	}

	static play(sound) {
		SoundPlayer.SoundLib[sound].play();
	}
}
