export const Key = {
	SPACE: 32,
	ARROW_LEFT: 37,
	ARROW_UP: 38,
	ARROW_RIGHT: 39,
	ARROW_DOWN: 40,

	NUM_0: 48,
	NUM_1: 49,
	NUM_2: 50,
	NUM_3: 51,
	NUM_4: 52,
	NUM_5: 53,
	NUM_6: 54,
	NUM_7: 55,
	NUM_8: 56,
	NUM_9: 57,
	SEMI_COLON: 59,
	EQUALS: 61,
	A: 65,
	B: 66,
	C: 67,
	D: 68,
	E: 69,
	F: 70,
	G: 71,
	H: 72,
	I: 73,
	J: 74,
	K: 75,
	L: 76,
	M: 77,
	N: 78,
	O: 79,
	P: 80,
	Q: 81,
	R: 82,
	S: 83,
	T: 84,
	U: 85,
	V: 86,
	W: 87,
	X: 88,
	Y: 89,
	Z: 90,
	NUM_PAD_0: 96,
	NUM_PAD_1: 97,
	NUM_PAD_2: 98,
	NUM_PAD_3: 99,
	NUM_PAD_4: 100,
	NUM_PAD_5: 101,
	NUM_PAD_6: 102,
	NUM_PAD_7: 103,
	NUM_PAD_8: 104,
	NUM_PAD_9: 105,
	MULTIPLY: 106,
	ADD: 107,
	SEPARATOR: 108,
	SUBTRACT: 109,
	DECIMAL: 110,
	DIVIDE: 111,
	COMMA: 188,
	PERIOD: 190,
	SLASH: 191,
	BACK_QUOTE: 192,
	OPEN_BRACKET: 219,
	BACK_SLASH: 220,
	CLOSE_BRACKET: 221,
	QUOTE: 222,
	META: 224
};