export class InputManager {
	pressedKey = null;

	static isPressing(key) {
		if (key === InputManager.pressedKey) {
			InputManager.pressedKey = null;
			return true;
		}

		return false;
	}

	static init() {
		InputManager.pressedKey = null;

		window.onkeydown = (ev) => {
			if (!ev.repeat) {
				InputManager.pressedKey = ev.keyCode;
			}
		};

		window.onkeyup = (ev) => {
			InputManager.pressedKey = null;
		}
	}
}
