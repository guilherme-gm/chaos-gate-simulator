import { GameState, MaxWords } from './GameState.js';
import { WordController } from './WordController.js';

export class WordUi {
	constructor(menuEle, finalWordEle, serverEle, word1Ele, word2Ele, word3Ele, confirmEle, yesNoEle) {
		this.menuEle = menuEle;
		this.confirmEle = confirmEle;
		this.yesNoEle = yesNoEle;
		this.finalWordEle = finalWordEle;
		this.serverEle = serverEle;
		/** @type {string[]} */
		this.wordEle = [word1Ele, word2Ele, word3Ele];

		this.controller = new WordController();
	}

	/* private */ createListChild(list, text, selected, cursor) {
		const newChild = document.createElement('li');

		newChild.innerText = text;
		if (selected) {
			newChild.classList.add('selected');
		}
		if (cursor) {
			newChild.classList.add('cursor');
		}

		list.appendChild(newChild);
	}

	/* private */ isCursorAt(row, col) {
		return this.controller.cursor.row === row && this.controller.cursor.col === col;
	}

	/* private */ renderServerList() {
		const newList = document.createElement('ul');

		if (GameState.canSelectServer) {
			for (let i = 0; i < GameState.serverList.length; i++) {
				this.createListChild(
					newList,
					GameState.serverList[i],
					i === this.controller.selection[0],
					this.isCursorAt(i, 0),
				);
			}
		} else {
			this.createListChild(newList, GameState.selectedServer, true, false);
		}

		this.serverEle.firstChild.replaceWith(newList);
	}

	/**
	 *
	 * @param {HTMLElement} element
	 * @param {*} wordList
	 */
	/* private */ renderWordList(element, wordIndex) {
		const newList = document.createElement('ul');
		const wordList = GameState.wordList[wordIndex];

		for (let i = 0; i < wordList.length; i++) {
			this.createListChild(
				newList,
				wordList[i].label,
				this.controller.isSelected(wordIndex + 1, i),
				this.isCursorAt(i, wordIndex + 1),
			);
		}

		element.firstChild.replaceWith(newList);
	}

	/* private */ renderConfirmWindow() {
		if (!this.controller.isConfirm) {
			this.confirmEle.classList.add('hidden');
			return;
		}

		this.confirmEle.classList.remove('hidden');
		const newList = document.createElement('ul');
		this.createListChild(newList, 'Yes', false, this.controller.yesNoRow === 0);
		this.createListChild(newList, 'No', false, this.controller.yesNoRow === 1);

		this.confirmEle.firstChild.replaceWith(newList);
	}

	/* public */ render() {
		if (this.controller.closed) {
			this.menuEle.classList.add('hidden');

			return;
		}
		this.controller.step();
		this.renderServerList();
		for (let i = 0; i < MaxWords; i++) {
			this.renderWordList(this.wordEle[i], i);
		}

		this.renderConfirmWindow();
	}
}

