import __wordList from "./words.js";

/**
 * Criando um enum de Scenes pra evitar strings por aí.
 * Infelizmente pro jeito que o JS funciona precisa fazer o nome repetido :/
 */
export const Scenes = {
	FreeMode: 'FreeMode',
	Town1: 'Town1',
	Town2: 'Town2',
};

/**
 * Criando um enum de Servers pra evitar strings por aí.
 * Infelizmente pro jeito que o JS funciona precisa fazer o nome repetido :/
 */
export const Servers = {
	Alfa: 'Alfa',
	Delta: 'Delta',

	Test: 'Test',
};

export const MaxWords = Object.keys(__wordList).length;

/**
 * Uma espécie de classe global para armazenar dados globais / o estado global do jogo.
 * Use para elementos altamente compartilhados / que precisam ser acessados para muitas lógicas diferentes.
 *
 * GG: Em geral, é meio anti-pattern ter uma god class assim, mas em games é difícil escapar mesmo.
 */
export class GameState {
	/** @type {{ label: string }[][]} */
	/* public readonly */ static wordList = [__wordList[0], __wordList[1], __wordList[2]];

	/**
	 * Lista de servidores selecionáveis.
	 * Aqui eu tou gerando a partir do "enum" de servers, filtrando o de Teste.
	 */
	/* public readonly */ static serverList = Object.keys(Servers)
		.filter(sv => sv !== 'Test');

	/* public */ static selectedServer = null;

	/** @type {{ label: string }[]} */
	/* public */ static selectedWords = [null, null, null];

	/* public */ static canSelectServer = false;



	/**
	 * Converte o nome da cena pro server equivalente
	 */
	/* private */ static sceneToServer(sceneName) {
		switch (sceneName) {
		case Scenes.FreeMode:
			return Servers.Test;

		case Scenes.Town1:
			return Servers.Alfa;

		case Scenes.Town2:
			return Servers.Delta;

		default:
			throw new Error("Scene doesn't have an associated server.");
		}
	}

	/* public */ static changeServer(server) {
		if (server === Servers.Test) {
			GameState.selectedServer = GameState.serverList[0];
		} else {
			GameState.selectedServer = server;
		}

		// GameState.selectedWords = [GameState.wordList[0][0], GameState.wordList[1][0], GameState.wordList[2][0]];
	}

	/**
	 * Refresh do estado do jogo quando a cena trocar
	 */
	/* public */ static changeScene(toScene) {
		const server = GameState.sceneToServer(toScene);

		GameState.canSelectServer = server === Servers.Test;
		GameState.changeServer(server);
	}
}
