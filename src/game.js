import { GameState, Scenes } from './GameState.js';
import { WordUi } from './WordUi.js';
import { InputManager} from  "./InputManager.js";
import { SoundPlayer } from "./SoundPlayer.js";

GameState.changeScene(Scenes.FreeMode);
InputManager.init();
SoundPlayer.init();

const sceneSelector = document.getElementById("scene");
Object.keys(Scenes).forEach((scene) => {
	const item = document.createElement('option');

	item.value = scene;
	item.innerText = scene;

	sceneSelector.appendChild(item);
});

function createGameUi() {
	return new WordUi(
		document.getElementById('menu'),
		document.getElementById('finalWord'),
		document.getElementById('world'),
		document.getElementById('word1'),
		document.getElementById('word2'),
		document.getElementById('word3'),
		document.getElementById('confirm'),
		document.getElementById('yesNo'),
	);
}

let gameUi = createGameUi();

sceneSelector.addEventListener('change', (ev) => {
	GameState.changeScene(ev.target.value);

	gameUi = createGameUi();
});

function gameLoop() {
	gameUi.render();

	window.requestAnimationFrame(gameLoop);
}

window.requestAnimationFrame(gameLoop);
